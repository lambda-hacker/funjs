module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.registerTask('default', ['concurrent:default']);

  return grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    connect: {
      server: {
        options: {
          port: 8000,
          keepalive: true,
          livereload: true,
          base: ['.']
        }
      }
    },
    watch: {
      reload: {
        files: ['*.js', '*.html']
      },
      options: {
        spawn: true,
        livereload: true
      }
    },
    concurrent: {
      default: ['connect', 'watch'],
      options: {
        logConcurrentOutput: true
      }
    }
  });
};
