function sayHello(message) {
  console.log(message  + this.name);
}

var msg = {
  name: 'pervez',
  sayHello: sayHello
};

sayHello('hello '); // function invocation: this == window(global object)
msg.sayHello('hi '); // method invocation: this == msg
// first param == this, function parameters passed
sayHello.call({ name: 'iqbal' }, 'hello ');
// first param == this, parameters passed in an arry
sayHello.apply({ name: 'iqbal' }, ['hello ']);
