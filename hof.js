var repeat = function(n, f) {
  for(var i = 0; i < n; ++i)
    f();
};

var range = function(start, stop) {
  var result = [];
  for(var i = start; i < stop; ++i)
    result.push(i);
  return result;
};

var each = function(arr, f) {
  for(var i = 0; i < arr.length; ++i)
    f(arr[i]);
};

var map = function(arr, f) {
  var mapped = [];
  each(arr, function(e) { mapped.push(f(e)); });
  return mapped;
};


var map_p = function(arr, f, pred) {
  var res = [];
  each(arr, function(e) { res.push(pred(e) ? f(e) : e);
  });
  return res;
};

var filter = function(arr, pred) {
  var filtered = [];
  each(arr, function(e) {
    if(pred(e))
      filtered.push(e);
  });

  return filtered;
};

var contains = function(arr, pred) {
  for(var i = 0; i < arr.length; ++i)
    if(pred(arr[i]))
       return true;
  return false;
};

// Zip
var zip = function (a1, a2) {
  var zipped = [];
  var minLen = a1.length <= a2.length ? a1.length : a2.length ;
  for (var i = 0; i < minLen; ++i) {
       zipped.push([a1[i], a2[i]]);
  }
  return zipped;
  
};

//zip with

var zip_with = function (a1, a2, f) {
  var res = [];
  var minLen = a1.length <= a2.length ? a1.length : a2.length ;
  for (var i = 0 ; i < minLen; ++i) {
    res.push( f(a1[i], a2[i]) );
  }
  return res;
};


// take
var take = function(n, arr) {
    var res = [];
    for (var i = 0; i < n && n > 0; ++i) {
        res.push(arr[i]);
    }
    return res;    
};

// drop
var drop = function(n, arr) {
    var res = [];
    if (n < 0) n = 0;
    for (var i = n; i < arr.length; ++i) {
        res.push(arr[i]);
    }
    return res;
};

// compare
var compare = function(a, b) { 
  return a < b ? -1 : (a===b ? 0 : 1);   
};

// merge 2 arrays
// sort both and find merge

var merge = function(a1, a2) {
  var res = [];
  if (a1 == null || a2 == null) {
    return undefined;
  }

  a1.sort(compare);
  a2.sort(compare);
  //console.log(a1);
  //console.log(a2);

  var l1 = a1.length;
  var l2 = a2.length;

  var i = 0, j = 0, k = 0;

  while ( k < l1 + l2 ) {
    if ( (j === l2 ) || (a1[i] <= a2[j] && i < l1)
        ) {
      res.push(a1[i++]);
      k++;
    }
    else if ( (i === l1) ||  (a2[j] < a1[i] && j < l2)) {
      res.push(a2[j++]);
      k++;
    }
  } 

  return res;
};

// remove duplicates in arr
// assuming arr not sorted, sort and remove duplicates
var remove_duplicates = function(arr) {  
  if (arr == null) return undefined;
  
  var res = [];
  arr.sort(compare);
  if (arr.length !== 0) res.push(arr[0]);
  
  for (var i = 1; i < arr.length; ++i ) {
    if (arr[i] != arr[i - 1])
      res.push(arr[i]);
  }
  return res;
};

// union of 2 arrays
// sort and find union

var union = function (a, b) {
  res = [];
  var s1 = remove_duplicates(a);
  var s2 = remove_duplicates(b);

  res = remove_duplicates( merge(s1, s2) );
  return res;
};


// intersect of 2 arrays
// sort and find intersection
var intersect = function(a, b) {
  res = [];
  var s1 = remove_duplicates(a);
  var s2 = remove_duplicates(b);
  //console.log(s1);
  //console.log(s2);

  var l1 = s1.length;
  var l2 = s2.length;

  var i = 0, j = 0;
  while (i < l1 && j < l2) {
    if (s1[i] === s2[j]) {
        res.push(s1[i]);
        ++i; ++j;
    }
    if (s1[i] < s2[j])   ++i;
    if (s2[j] < s1[i])   ++j;
  }
  return res;
};




